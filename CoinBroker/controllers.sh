
grails generate-all com.coinbroker.Account               -force
grails generate-all com.coinbroker.BalanceEntry         -force
grails generate-all com.coinbroker.BuyTokenLot           -force
grails generate-all com.coinbroker.Customer              -force
grails generate-all com.coinbroker.ExternalCreditCardAccount       -force
grails generate-all com.coinbroker.ExternalBankAccount   -force
grails generate-all com.coinbroker.ExternalDebitCardAccount  -force
grails generate-all com.coinbroker.ExternalWalletAccount -force
grails generate-all com.coinbroker.Bill               -force
grails generate-all com.coinbroker.BillItem           -force
grails generate-all com.coinbroker.Note                  -force
grails generate-all com.coinbroker.SellTokenLot          -force
grails generate-all com.coinbroker.TokenLot              -force
grails generate-all com.coinbroker.Transaction        -force
grails generate-all com.coinbroker.WalletAccount         -force
