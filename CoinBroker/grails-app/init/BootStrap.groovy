import com.coinbroker.*

class BootStrap {
    def springSecurityService

    def init = { servletContext ->
        def userRole = new Role(authority: 'ROLE_USER').save()
        def me = new User(username: 'admin', password: 'admin').save()
        UserRole.create me, userRole
 /*
  * Random Passwords
  * e5T4bLS8q38CeXtW
  * yQF9Me4aDWraudNM
  * dnQUL25TMDJJqSv7
  */
        def marketplace = new User(username: 'marketplace', password: 'e5T4bLS8q38CeXtW').save()
        UserRole.create marketplace, userRole

        def wellsaapp = new User(username: 'wellsaapp', password: 'yQF9Me4aDWraudNM').save()
        UserRole.create wellsaapp, userRole

        UserRole.withSession {
            it.flush()
            it.clear()
        }

        def wellsaCustomer = new Customer(
                type: Customer.TYPE_BUSINESS,
                status: Constants.STATUS_ENABLED,
                accountingCurrency: Constants.CURRENCY_DOLLAR,
                kycAmlId: 'self',
                isVendor: true,
                isAppUser: false,
                contactName: new PersonName( first: 'Main Account', last: 'Wellsa' ),
                businessName: 'Wellsa',
                billingAddress: new Address(
                        address1: '47071 Bayside Parkway', city: 'Fremont',
                        state: 'CA', postalCode: '94538', country: 'USA',),
                phoneNumber: '800-555-1212',
                email: 'wellsame@wellsa.me',
        ).save()

        def marketPlaceCustomer = new Customer(
                type: Customer.TYPE_BUSINESS,
                status: Constants.STATUS_ENABLED,
                accountingCurrency: Constants.CURRENCY_DOLLAR,
                kycAmlId: 'self',
                isVendor: true,
                isAppUser: false,
                contactName: new PersonName( first: 'Market Place', last: 'Wellsa' ),
                businessName: 'Wellsa',
                billingAddress: new Address(
                        address1: '47071 Bayside Parkway', city: 'Fremont',
                        state: 'CA', postalCode: '94538', country: 'USA',),
                phoneNumber: '800-555-1212',
                email: 'wellsame@wellsa.me',
        ).save()

        def myName = new PersonName(
                first:  'Sam',
                middle: 'I',
                last:   'Am',
        )

        def myAddr = new Address(
                address1: '100 main st.',
                city: 'Rome',
                state: 'CA',
                postalCode: '94000',
                country: 'USA',
        )


        def jake = new Customer(
//                customerState: Constants.STATUS_ENABLED,
                type: Customer.TYPE_INDIVIDUAL,
                kycAmlId: '44575479',
                contactName: myName,
                billingAddress: myAddr,
                phoneNumber: '867-5309',
                email: 'sam@iam.com'
        ).save()

        jake.businessName = 'acme'
        jake.save()
        jake.contactName.middle = 'i'
        jake.save()

        Customer.withSession {
            it.flush()
            it.clear()
        }

    }

    def destroy = {
    }

}
