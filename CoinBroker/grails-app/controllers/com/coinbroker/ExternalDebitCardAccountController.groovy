package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ExternalDebitCardAccountController {

    ExternalDebitCardAccountService externalDebitCardAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond externalDebitCardAccountService.list(params), model:[externalDebitCardAccountCount: externalDebitCardAccountService.count()]
    }

    def show(Long id) {
        respond externalDebitCardAccountService.get(id)
    }

    def create() {
        respond new ExternalDebitCardAccount(params)
    }

    def save(ExternalDebitCardAccount externalDebitCardAccount) {
        if (externalDebitCardAccount == null) {
            notFound()
            return
        }

        try {
            externalDebitCardAccountService.save(externalDebitCardAccount)
        } catch (ValidationException e) {
            respond externalDebitCardAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'externalDebitCardAccount.label', default: 'ExternalDebitCardAccount'), externalDebitCardAccount.id])
                redirect externalDebitCardAccount
            }
            '*' { respond externalDebitCardAccount, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond externalDebitCardAccountService.get(id)
    }

    def update(ExternalDebitCardAccount externalDebitCardAccount) {
        if (externalDebitCardAccount == null) {
            notFound()
            return
        }

        try {
            externalDebitCardAccountService.save(externalDebitCardAccount)
        } catch (ValidationException e) {
            respond externalDebitCardAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'externalDebitCardAccount.label', default: 'ExternalDebitCardAccount'), externalDebitCardAccount.id])
                redirect externalDebitCardAccount
            }
            '*'{ respond externalDebitCardAccount, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        externalDebitCardAccountService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'externalDebitCardAccount.label', default: 'ExternalDebitCardAccount'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'externalDebitCardAccount.label', default: 'ExternalDebitCardAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
