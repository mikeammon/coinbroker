package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BuyTokenLotController {

    BuyTokenLotService buyTokenLotService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond buyTokenLotService.list(params), model:[buyTokenLotCount: buyTokenLotService.count()]
    }

    def show(Long id) {
        respond buyTokenLotService.get(id)
    }

    def create() {
        respond new BuyTokenLot(params)
    }

    def save(BuyTokenLot buyTokenLot) {
        if (buyTokenLot == null) {
            notFound()
            return
        }

        try {
            buyTokenLotService.save(buyTokenLot)
        } catch (ValidationException e) {
            respond buyTokenLot.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'buyTokenLot.label', default: 'BuyTokenLot'), buyTokenLot.id])
                redirect buyTokenLot
            }
            '*' { respond buyTokenLot, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond buyTokenLotService.get(id)
    }

    def update(BuyTokenLot buyTokenLot) {
        if (buyTokenLot == null) {
            notFound()
            return
        }

        try {
            buyTokenLotService.save(buyTokenLot)
        } catch (ValidationException e) {
            respond buyTokenLot.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'buyTokenLot.label', default: 'BuyTokenLot'), buyTokenLot.id])
                redirect buyTokenLot
            }
            '*'{ respond buyTokenLot, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        buyTokenLotService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'buyTokenLot.label', default: 'BuyTokenLot'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'buyTokenLot.label', default: 'BuyTokenLot'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
