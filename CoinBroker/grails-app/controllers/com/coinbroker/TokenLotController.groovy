package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TokenLotController {

    TokenLotService tokenLotService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond tokenLotService.list(params), model:[tokenLotCount: tokenLotService.count()]
    }

    def show(Long id) {
        respond tokenLotService.get(id)
    }

    def create() {
        respond new TokenLot(params)
    }

    def save(TokenLot tokenLot) {
        if (tokenLot == null) {
            notFound()
            return
        }

        try {
            tokenLotService.save(tokenLot)
        } catch (ValidationException e) {
            respond tokenLot.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tokenLot.label', default: 'TokenLot'), tokenLot.id])
                redirect tokenLot
            }
            '*' { respond tokenLot, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond tokenLotService.get(id)
    }

    def update(TokenLot tokenLot) {
        if (tokenLot == null) {
            notFound()
            return
        }

        try {
            tokenLotService.save(tokenLot)
        } catch (ValidationException e) {
            respond tokenLot.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tokenLot.label', default: 'TokenLot'), tokenLot.id])
                redirect tokenLot
            }
            '*'{ respond tokenLot, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        tokenLotService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'tokenLot.label', default: 'TokenLot'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tokenLot.label', default: 'TokenLot'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
