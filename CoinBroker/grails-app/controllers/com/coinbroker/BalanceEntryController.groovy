package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BalanceEntryController {

    BalanceEntryService balanceEntryService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond balanceEntryService.list(params), model:[balanceEntryCount: balanceEntryService.count()]
    }

    def show(Long id) {
        respond balanceEntryService.get(id)
    }

    def create() {
        respond new BalanceEntry(params)
    }

    def save(BalanceEntry balanceEntry) {
        if (balanceEntry == null) {
            notFound()
            return
        }

        try {
            balanceEntryService.save(balanceEntry)
        } catch (ValidationException e) {
            respond balanceEntry.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'balanceEntry.label', default: 'BalanceEntry'), balanceEntry.id])
                redirect balanceEntry
            }
            '*' { respond balanceEntry, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond balanceEntryService.get(id)
    }

    def update(BalanceEntry balanceEntry) {
        if (balanceEntry == null) {
            notFound()
            return
        }

        try {
            balanceEntryService.save(balanceEntry)
        } catch (ValidationException e) {
            respond balanceEntry.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'balanceEntry.label', default: 'BalanceEntry'), balanceEntry.id])
                redirect balanceEntry
            }
            '*'{ respond balanceEntry, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        balanceEntryService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'balanceEntry.label', default: 'BalanceEntry'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'balanceEntry.label', default: 'BalanceEntry'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
