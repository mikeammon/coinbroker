package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ExternalCreditCardAccountController {

    ExternalCreditCardAccountService externalCreditCardAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond externalCreditCardAccountService.list(params), model:[externalCreditCardAccountCount: externalCreditCardAccountService.count()]
    }

    def show(Long id) {
        respond externalCreditCardAccountService.get(id)
    }

    def create() {
        respond new ExternalCreditCardAccount(params)
    }

    def save(ExternalCreditCardAccount externalCreditCardAccount) {
        if (externalCreditCardAccount == null) {
            notFound()
            return
        }

        try {
            externalCreditCardAccountService.save(externalCreditCardAccount)
        } catch (ValidationException e) {
            respond externalCreditCardAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'externalCreditCardAccount.label', default: 'ExternalCreditCardAccount'), externalCreditCardAccount.id])
                redirect externalCreditCardAccount
            }
            '*' { respond externalCreditCardAccount, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond externalCreditCardAccountService.get(id)
    }

    def update(ExternalCreditCardAccount externalCreditCardAccount) {
        if (externalCreditCardAccount == null) {
            notFound()
            return
        }

        try {
            externalCreditCardAccountService.save(externalCreditCardAccount)
        } catch (ValidationException e) {
            respond externalCreditCardAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'externalCreditCardAccount.label', default: 'ExternalCreditCardAccount'), externalCreditCardAccount.id])
                redirect externalCreditCardAccount
            }
            '*'{ respond externalCreditCardAccount, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        externalCreditCardAccountService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'externalCreditCardAccount.label', default: 'ExternalCreditCardAccount'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'externalCreditCardAccount.label', default: 'ExternalCreditCardAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
