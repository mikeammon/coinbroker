package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BillController {

    BillService invoiceService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond invoiceService.list(params), model:[invoiceCount: invoiceService.count()]
    }

    def show(Long id) {
        respond invoiceService.get(id)
    }

    def create() {
        respond new Bill(params)
    }

    def save(Bill invoice) {
        if (invoice == null) {
            notFound()
            return
        }

        try {
            invoiceService.save(invoice)
        } catch (ValidationException e) {
            respond invoice.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label', default: 'Bill'), invoice.id])
                redirect invoice
            }
            '*' { respond invoice, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond invoiceService.get(id)
    }

    def update(Bill invoice) {
        if (invoice == null) {
            notFound()
            return
        }

        try {
            invoiceService.save(invoice)
        } catch (ValidationException e) {
            respond invoice.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'invoice.label', default: 'Bill'), invoice.id])
                redirect invoice
            }
            '*'{ respond invoice, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        invoiceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoice.label', default: 'Bill'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Bill'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
