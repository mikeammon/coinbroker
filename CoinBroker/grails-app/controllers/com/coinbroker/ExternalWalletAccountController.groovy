package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ExternalWalletAccountController {

    ExternalWalletAccountService externalWalletAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond externalWalletAccountService.list(params), model:[externalWalletAccountCount: externalWalletAccountService.count()]
    }

    def show(Long id) {
        respond externalWalletAccountService.get(id)
    }

    def create() {
        respond new ExternalWalletAccount(params)
    }

    def save(ExternalWalletAccount externalWalletAccount) {
        if (externalWalletAccount == null) {
            notFound()
            return
        }

        try {
            externalWalletAccountService.save(externalWalletAccount)
        } catch (ValidationException e) {
            respond externalWalletAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'externalWalletAccount.label', default: 'ExternalWalletAccount'), externalWalletAccount.id])
                redirect externalWalletAccount
            }
            '*' { respond externalWalletAccount, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond externalWalletAccountService.get(id)
    }

    def update(ExternalWalletAccount externalWalletAccount) {
        if (externalWalletAccount == null) {
            notFound()
            return
        }

        try {
            externalWalletAccountService.save(externalWalletAccount)
        } catch (ValidationException e) {
            respond externalWalletAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'externalWalletAccount.label', default: 'ExternalWalletAccount'), externalWalletAccount.id])
                redirect externalWalletAccount
            }
            '*'{ respond externalWalletAccount, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        externalWalletAccountService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'externalWalletAccount.label', default: 'ExternalWalletAccount'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'externalWalletAccount.label', default: 'ExternalWalletAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
