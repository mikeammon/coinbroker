package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BillItemController {

    BillItemService invoiceItemService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond invoiceItemService.list(params), model:[invoiceItemCount: invoiceItemService.count()]
    }

    def show(Long id) {
        respond invoiceItemService.get(id)
    }

    def create() {
        respond new BillItem(params)
    }

    def save(BillItem invoiceItem) {
        if (invoiceItem == null) {
            notFound()
            return
        }

        try {
            invoiceItemService.save(invoiceItem)
        } catch (ValidationException e) {
            respond invoiceItem.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceItem.label', default: 'BillItem'), invoiceItem.id])
                redirect invoiceItem
            }
            '*' { respond invoiceItem, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond invoiceItemService.get(id)
    }

    def update(BillItem invoiceItem) {
        if (invoiceItem == null) {
            notFound()
            return
        }

        try {
            invoiceItemService.save(invoiceItem)
        } catch (ValidationException e) {
            respond invoiceItem.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'invoiceItem.label', default: 'BillItem'), invoiceItem.id])
                redirect invoiceItem
            }
            '*'{ respond invoiceItem, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        invoiceItemService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoiceItem.label', default: 'BillItem'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceItem.label', default: 'BillItem'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
