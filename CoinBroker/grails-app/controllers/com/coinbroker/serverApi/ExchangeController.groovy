package com.coinbroker.serverApi

import groovy.json.JsonSlurper
import grails.converters.*

class ExchangeController {
    def getExchangeRates() {
        def result =         [
                success : [
                        status : 'success',
                        bidQuote : [
                                quoteId : '5576454',
                                quoteType : 'bid',
                                dollarsPerCoin : 3.45,
                                expiration : 1527400532606,
                        ],
                        askQuote : [
                                quoteId : '5576454',
                                quoteType : 'ask',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                        marketPrice : [
                                dollarsPerCoin : 3.55,
                                timeStamp : 1527400532606,
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def getExchangePrice() {
        def result =         [
                success : [
                        status : 'success',
                        coinAmount : 20.0,
                        dollarAmount : 73.0,
                        exchangeQuote : [
                                quoteId : '5576454',
                                quoteType : 'ask',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def buyCoins() {
        def result =         [
                success : [
                        status : 'success',
                ],
                quoteExpired : [
                        status : 'quoteExpired',
                        errorDescription : 'Exchange quote expired',
                        exchangeQuote : [
                                quoteId : '5576454',
                                quoteType : 'ask',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                ],
                externalAccountError : [
                        status : 'externalAccountError',
                        errorDescription : 'Credit card account has expired',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def sellCoins() {
        def result =         [
                success : [
                        status : 'success',
                ],
                quoteExpired : [
                        status : 'quoteExpired',
                        errorDescription : 'Exchange quote expired',
                        exchangeQuote : [
                                quoteId : '5576454',
                                quoteType : 'bid',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                ],
                insufficientFunds : [
                        status : 'insufficientFunds',
                        errorDescription : 'Coin amount exceeds available balance',
                        availableBalance : 2.45,
                        coinAmount : 1.85,
                ],
                externalAccountError : [
                        status : 'externalAccountError',
                        errorDescription : 'External account access failed',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def getWalletKey() {
        def result =         [
                success : [
                        status : 'success',
                        walletKey : '668513076546733876153',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
