package com.coinbroker.serverApi

import com.coinbroker.Address
import com.coinbroker.Constants
import com.coinbroker.Customer
import com.coinbroker.CustomerService
import com.coinbroker.PersonName
import com.coinbroker.WalletAccount
import com.coinbroker.WalletAccountService
import grails.validation.ValidationException
import groovy.json.JsonParserType
import groovy.json.JsonSlurper
import grails.converters.*
import org.grails.web.json.JSONObject

class CoinAccountController {

    CustomerService customerService
    WalletAccountService walletAccountService

    def createAccountTesting() {
        def slurper = new JsonSlurper().setType(JsonParserType.LAX)
        def bodyText =  request.reader.text
        def jsonResp = bodyText.isEmpty() ? [:] : slurper.parseText(bodyText)

        def customerParams = [
                type: jsonResp.type == Customer.TYPE_BUSINESS ? Customer.TYPE_BUSINESS : Customer.TYPE_INDIVIDUAL,
                accountingCurrency: jsonResp.accountingCurrency ?: Constants.CURRENCY_DOLLAR,
                kycAmlId: jsonResp.kycAmlId,
                isAppUser: jsonResp.isAppUser ?: false,
                isVendor: jsonResp.isVendor ?: false,
                contactName: new PersonName( jsonResp.contact.contactName ),
                businessName: jsonResp.businessName,
                billingAddress: new Address( jsonResp.contact.billingAddress ),
                phoneNumber: jsonResp.contact.phoneNumber,
                email: jsonResp.contact.email,
                appUsageLevel: jsonResp.appUsageLevel,
        ]


        def customer = new Customer( customerParams )

        def result = [
                status : 'approved',
                customerId : customer.getId(),
        ]

        render result as JSON
    }

    def createAccount() {
        def result =         [
                success : [
                        status : 'approved',
                        customerId : '5004020',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def updateAccount() {
        def result =         [
                success : [
                        status : 'approved',
                        customerId : '5004020',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def updateBillingAddress() {
        def result =         [
                success : [
                        status : 'success',
                ],
                inputError : [
                        status : 'inputError',
                        previousInputs : 'Returns input fields supplied with call',
                        errorFields : [
                                address1 : [
                                        errorCode : 323,
                                        errorReason : 'Address not found',
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def summaryOfAccounts() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        availableForPurchases : 13.6,
                        coinAccounts : [
                                wallet : [
                                        accountId : '50060087',
                                        type : 'wallet',
                                        currency : 'WellCoin',
                                        currentBalance : 22.5,
                                        availableBalance : 7.3,
                                ],
                                credit : [
                                        accountId : '50060088',
                                        type : 'credit',
                                        currency : 'WellCoin',
                                        currentBalance : 30.4,
                                        availableBalance : 12.6,
                                        paymentDueDate : 1527404532606,
                                        minimumPaymentDue : 13.80,
                                        lastStatementBalance : 27.6,
                                        creditTerms : [
                                                marginRate : 2.0,
                                                interestRate : 0.145,
                                                maxCreditLimit : 45.0,
                                                creditLimit : 43.0,
                                        ],
                                ],
                        ],
                        externalAccounts : [
                                'VISA Bank of America' : [
                                        name : 'VISA Bank of America',
                                        externalAccountId : '455576',
                                        accountType : 'visa',
                                        currency : 'USD',
                                        bankName : 'Bank of America',
                                        default : true,
                                        last4digits : '4556',
                                        status : 'active',
                                ],
                                'Checking Bank of America' : [
                                        name : 'Checking Bank of America',
                                        externalAccountId : '455575',
                                        accountType : 'bank_checking',
                                        currency : 'USD',
                                        bankName : 'Bank of America',
                                        status : 'active',
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def accountActivity() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504024',
                        currency : 'WellCoin',
                        activity : [
                                [
                                        date : 1527400532606,
                                        description : 'Payment received',
                                        principal : 3.0,
                                        amount : 3.0,
                                        balance : 22.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'Payment received',
                                        principal : 5.0,
                                        amount : 5.0,
                                        balance : 17.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'PAMF - co-pay',
                                        principal : -2.0,
                                        amount : -2.0,
                                        balance : 19.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'Interest',
                                        interest : -0.62,
                                        amount : -0.62,
                                        balance : 20.04,
                                ],
                                [
                                        date : 1527798532606,
                                        description : 'Late payment fee',
                                        fees : -0.5,
                                        amount : -0.5,
                                        balance : 20.54,
                                ],
                                [
                                        date : 1527798532606,
                                        description : 'Market Place - GNC',
                                        principal : -3.0,
                                        amount : -3.0,
                                        balance : 23.54,
                                ],
                        ],
                ],
                wallet : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504004',
                        currency : 'WellCoin',
                        activity : [
                                [
                                        date : 1527400532606,
                                        description : 'Credit payment',
                                        amount : -3.0,
                                        balance : 22.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'Credit payment',
                                        amount : -5.0,
                                        balance : 17.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'Purchace coins from Checking Bank of America',
                                        amount : 2.0,
                                        balance : 19.42,
                                ],
                                [
                                        date : 1527476532606,
                                        description : 'Mining payment',
                                        amount : 0.62,
                                        balance : 20.04,
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def transferCoins() {
        def result =         [
                success : [
                        status : 'success',
                ],
                insufficientCoins : [
                        status : 'insufficientFunds',
                        errorDescription : 'Coin amount exceeds available balance',
                        availableBalance : 2.45,
                        coinAmount : 1.85,
                ],
                externalAccountError : [
                        status : 'externalAccountError',
                        errorDescription : 'External account access failed',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
