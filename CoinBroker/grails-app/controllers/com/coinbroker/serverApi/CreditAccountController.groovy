package com.coinbroker.serverApi

import groovy.json.JsonSlurper
import grails.converters.*

class CreditAccountController {
    def createCreditApplication() {
        def result =         [
                success : [
                        status : 'approved',
                        customerId : '5004020',
                        accounts : [
                                wallet : [
                                        accountId : '50060087',
                                        type : 'wallet',
                                ],
                                credit : [
                                        accountId : '50060088',
                                        type : 'credit',
                                        creditTerms : [
                                                marginRate : 2.0,
                                                interestRate : 0.145,
                                                maxCreditLimit : 45.0,
                                        ],
                                ],
                        ],
                ],
                declineLegal : [
                        status : 'declined',
                        declineCode : 4005,
                        declineReason : 'We are prohibited from offering you an account at this time',
                ],
                inputError : [
                        status : 'inputError',
                        applicationId : 46755,
                        previousInputs : 'Returns input fields supplied with call',
                        fields : [
                                ssn : [
                                        code : 456,
                                        description : 'Should have 9 numbers',
                                ],
                        ],
                ],
                requestAdditionalInput : [
                        status : 'requestAdditionalInput',
                        applicationId : 46755,
                        previousInputs : 'Returns input fields supplied with call',
                        fields : [
                                'previousAddress',
                                'driversLicense',
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def updateCreditApplication() {
        def result =         [
                success : [
                        status : 'approved',
                        customerId : '5004020',
                        accounts : [
                                wallet : [
                                        accountId : '50060087',
                                        type : 'wallet',
                                ],
                                credit : [
                                        accountId : '50060088',
                                        type : 'credit',
                                        creditTerms : [
                                                marginRate : 2.0,
                                                interestRate : 0.145,
                                                maxCreditLimit : 45.0,
                                        ],
                                ],
                        ],
                ],
                declineLegal : [
                        status : 'declined',
                        declineCode : 4005,
                        declineReason : 'We are prohibited from offering you an account at this time',
                ],
                inputError : [
                        status : 'inputError',
                        applicationId : 46755,
                        previousInputs : 'Returns input fields supplied with call',
                        fields : [
                                ssn : [
                                        code : 456,
                                        description : 'Should have 9 numbers',
                                ],
                        ],
                ],
                requestAdditionalInput : [
                        status : 'requestAdditionalInput',
                        applicationId : 46755,
                        previousInputs : 'Returns input fields supplied with call',
                        fields : [
                                'previousAddress',
                                'driversLicense',
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def creditTerms() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504024',
                        creditTerms : [
                                marginRate : 2.0,
                                interestRate : 0.145,
                                maxCreditLimit : 45.0,
                                creditLimit : 43.0,
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def creditAccountStatus() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504024',
                        currency : 'WellCoin',
                        activity : [
                                previousBalance : [
                                        date : 1527404532606,
                                        amount : 25.42,
                                ],
                                paymentReceived : [
                                        [
                                                date : 1527476532606,
                                                amount : 5.0,
                                        ],
                                        [
                                                date : 1527400532606,
                                                amount : 3.0,
                                        ],
                                ],
                                fees : [
                                        [
                                                date : 1527476532606,
                                                amount : 0.62,
                                                description : 'Interest',
                                        ],
                                        [
                                                date : 1527798532606,
                                                amount : 0.5,
                                                description : 'Late payment fee',
                                        ],
                                ],
                                chargesThisPeriod : [
                                        [
                                                date : 1527476532606,
                                                amount : 2.0,
                                                description : 'PAMF - co-pay',
                                        ],
                                        [
                                                date : 1527798532606,
                                                amount : 3.0,
                                                description : 'Market Place - GNC',
                                        ],
                                ],
                                newBalance : 23.54,
                                paymentDueDate : 1527798532606,
                                minimumPaymentDue : 2.74,
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def payCreditAccount() {
        def result =         [
                success : [
                        status : 'success',
                ],
                minPaymentError : [
                        status : 'minPaymentError',
                        errorDescription : 'Payment is less than minimum required payment',
                        minPaymentDue : 4.32,
                        paymentAmount : 2.47,
                ],
                insufficientFunds : [
                        status : 'insufficientFunds',
                        errorDescription : 'Payment exceeds available funds',
                        paymentAmount : 4.3,
                        availableBalance : 2.45,
                        coinAmount : 1.85,
                        dollarAmount : 6.75,
                        exchangeQuote : [
                                quoteId : '5576454',
                                quoteType : 'ask',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                ],
                quoteExpired : [
                        status : 'quoteExpired',
                        errorDescription : 'Exchange quote expired',
                        coinAmount : 1.85,
                        dollarAmount : 6.75,
                        exchangeQuote : [
                                quoteId : '5576454',
                                quoteType : 'ask',
                                dollarsPerCoin : 3.65,
                                expiration : 1527400532606,
                        ],
                ],
                externalAccountError : [
                        status : 'externalAccountError',
                        errorDescription : 'Credit card account has expired',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
