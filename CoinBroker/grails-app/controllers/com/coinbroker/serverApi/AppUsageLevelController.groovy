package com.coinbroker.serverApi

import groovy.json.JsonSlurper
import grails.converters.*

class AppUsageLevelController {
    def setAppUsageLevel() {
        def result =         [
                success : [
                        status : 'success',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def getAppUsageLevel() {
        def result =         [
                success : [
                        status : 'success',
                        appUsageLevel : 3,
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def creditTermsForUsageLevel() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504024',
                        creditTerms : [
                                marginRate : 2.0,
                                interestRate : 0.145,
                                maxCreditLimit : 45.0,
                                creditLimit : 43.0,
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def creditTermsForAllUsageLevels() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        accountId : '5504024',
                        levels : [
                                0 : [
                                        creditTerms : [
                                                marginRate : 2.0,
                                                interestRate : 0.145,
                                                maxCreditLimit : 45.0,
                                                creditLimit : 43.0,
                                        ],
                                ],
                                1 : [
                                        creditTerms : [
                                                marginRate : 2.2,
                                                interestRate : 0.135,
                                                maxCreditLimit : 49.5,
                                                creditLimit : 47.3,
                                        ],
                                ],
                                2 : [
                                        creditTerms : [
                                                marginRate : 2.5,
                                                interestRate : 0.13,
                                                maxCreditLimit : 56.25,
                                                creditLimit : 53.75,
                                        ],
                                ],
                                3 : [
                                        creditTerms : [
                                                marginRate : 2.8,
                                                interestRate : 0.125,
                                                maxCreditLimit : 45.0,
                                                creditLimit : 43.0,
                                        ],
                                ],
                                4 : [
                                        creditTerms : [
                                                marginRate : 3.0,
                                                interestRate : 0.12,
                                                maxCreditLimit : 67.5,
                                                creditLimit : 64.5,
                                        ],
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
