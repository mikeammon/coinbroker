package com.coinbroker.serverApi

import groovy.json.JsonSlurper
import grails.converters.*

class ExternalAccountController {
    def listExternalAccounts() {
        def result =         [
                success : [
                        status : 'success',
                        customerId : '5004020',
                        externalAccounts : [
                                'VISA Bank of America' : [
                                        name : 'VISA Bank of America',
                                        externalAccountId : '455576',
                                        accountType : 'visa',
                                        currency : 'USD',
                                        bankName : 'Bank of America',
                                        default : true,
                                        last4digits : '4556',
                                        status : 'active',
                                ],
                                'Checking Bank of America' : [
                                        name : 'Checking Bank of America',
                                        externalAccountId : '455575',
                                        accountType : 'bank_checking',
                                        currency : 'USD',
                                        bankName : 'Bank of America',
                                        status : 'active',
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def addExternalAccount() {
        def result =         [
                success : [
                        status : 'success',
                        externalAccountId : '455576',
                ],
                inputError : [
                        status : 'inputError',
                        previousInputs : 'Returns input fields supplied with call',
                        errorDescription : 'Input field errors',
                        errorFields : [
                                cardNumber : [
                                        errorCode : 557,
                                        errorReason : 'Invalid card format',
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def removeExternalAccount() {
        def result =         [
                success : [
                        status : 'success',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def updateExternalAccount() {
        def result =         [
                success : [
                        status : 'success',
                        externalAccountId : '455576',
                ],
                inputError : [
                        status : 'error',
                        previousInputs : 'Returns input fields supplied with call',
                        errorDescription : 'Input error',
                        errorFields : [
                                cardNumber : [
                                        errorCode : 557,
                                        errorReason : 'Invalid card format',
                                ],
                        ],
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def updateExternalAccountOrder() {
        def result =         [
                success : [
                        status : 'success',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
