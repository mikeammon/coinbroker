package com.coinbroker.serverApi

import groovy.json.JsonSlurper
import grails.converters.*

class BillingController {
    def billCustomer() {
        def result =         [
                success : [
                        status : 'pending',
                        billId : '778548990',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def billStatus() {
        def result =         [
                success : [
                        status : 'success',
                        billStatus : 'paid',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

    def chargeCustomer() {
        def result =         [
                success : [
                        status : 'paid',
                        billId : '778548990',
                ],
        ]

        if (result[params.test]) {
            render result[params.test] as JSON
        } else {
            render result.entrySet()[0]?.value as JSON
        }
    }

}
