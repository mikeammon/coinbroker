package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ExternalBankAccountController {

    ExternalBankAccountService externalBankAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond externalBankAccountService.list(params), model:[externalBankAccountCount: externalBankAccountService.count()]
    }

    def show(Long id) {
        respond externalBankAccountService.get(id)
    }

    def create() {
        respond new ExternalBankAccount(params)
    }

    def save(ExternalBankAccount externalBankAccount) {
        if (externalBankAccount == null) {
            notFound()
            return
        }

        try {
            externalBankAccountService.save(externalBankAccount)
        } catch (ValidationException e) {
            respond externalBankAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'externalBankAccount.label', default: 'ExternalBankAccount'), externalBankAccount.id])
                redirect externalBankAccount
            }
            '*' { respond externalBankAccount, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond externalBankAccountService.get(id)
    }

    def update(ExternalBankAccount externalBankAccount) {
        if (externalBankAccount == null) {
            notFound()
            return
        }

        try {
            externalBankAccountService.save(externalBankAccount)
        } catch (ValidationException e) {
            respond externalBankAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'externalBankAccount.label', default: 'ExternalBankAccount'), externalBankAccount.id])
                redirect externalBankAccount
            }
            '*'{ respond externalBankAccount, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        externalBankAccountService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'externalBankAccount.label', default: 'ExternalBankAccount'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'externalBankAccount.label', default: 'ExternalBankAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
