package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SellTokenLotController {

    SellTokenLotService sellTokenLotService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond sellTokenLotService.list(params), model:[sellTokenLotCount: sellTokenLotService.count()]
    }

    def show(Long id) {
        respond sellTokenLotService.get(id)
    }

    def create() {
        respond new SellTokenLot(params)
    }

    def save(SellTokenLot sellTokenLot) {
        if (sellTokenLot == null) {
            notFound()
            return
        }

        try {
            sellTokenLotService.save(sellTokenLot)
        } catch (ValidationException e) {
            respond sellTokenLot.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'sellTokenLot.label', default: 'SellTokenLot'), sellTokenLot.id])
                redirect sellTokenLot
            }
            '*' { respond sellTokenLot, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond sellTokenLotService.get(id)
    }

    def update(SellTokenLot sellTokenLot) {
        if (sellTokenLot == null) {
            notFound()
            return
        }

        try {
            sellTokenLotService.save(sellTokenLot)
        } catch (ValidationException e) {
            respond sellTokenLot.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'sellTokenLot.label', default: 'SellTokenLot'), sellTokenLot.id])
                redirect sellTokenLot
            }
            '*'{ respond sellTokenLot, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        sellTokenLotService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'sellTokenLot.label', default: 'SellTokenLot'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'sellTokenLot.label', default: 'SellTokenLot'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
