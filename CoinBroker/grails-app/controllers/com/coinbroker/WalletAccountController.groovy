package com.coinbroker

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class WalletAccountController {

    WalletAccountService walletAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond walletAccountService.list(params), model:[walletAccountCount: walletAccountService.count()]
    }

    def show(Long id) {
        respond walletAccountService.get(id)
    }

    def create() {
        respond new WalletAccount(params)
    }

    def save(WalletAccount walletAccount) {
        if (walletAccount == null) {
            notFound()
            return
        }

        try {
            walletAccountService.save(walletAccount)
        } catch (ValidationException e) {
            respond walletAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'walletAccount.label', default: 'WalletAccount'), walletAccount.id])
                redirect walletAccount
            }
            '*' { respond walletAccount, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond walletAccountService.get(id)
    }

    def update(WalletAccount walletAccount) {
        if (walletAccount == null) {
            notFound()
            return
        }

        try {
            walletAccountService.save(walletAccount)
        } catch (ValidationException e) {
            respond walletAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'walletAccount.label', default: 'WalletAccount'), walletAccount.id])
                redirect walletAccount
            }
            '*'{ respond walletAccount, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        walletAccountService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'walletAccount.label', default: 'WalletAccount'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'walletAccount.label', default: 'WalletAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
