package com.coinbroker

/**
 * BuyTokenLot
 * Lot of tokens from a buy or transfer into wallet
 */
class BuyTokenLot extends TokenLot {
    static hasMany = [sellLots: SellTokenLot]

    boolean depleted // True if all token in the lot were sold or used

    static belongsTo = [buyAccount: Account]

    static mapping = {
        depleted sqlType: "tinyint(1)"
    }

    public BuyTokenLot() {
        super()
        // Defaults
        action = ACTION_BUY
        depleted = false
    }

    BigDecimal getRemaining() {
        def amountLeft = amount - sellLots.collect { it.amount }.sum()
        if (amountLeft <= 0) { // should have error if amountLeft < 0
            this.depleted = true
            this.save()
        }
        amountLeft
    }

}
