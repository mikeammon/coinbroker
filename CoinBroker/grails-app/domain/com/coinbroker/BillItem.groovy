package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * BillItem
 * Items included in an Bill
 */
class BillItem implements Auditable, Stampable, Notable {
    String description // Item Description
    BigDecimal unitPrice   // Item Price
    BigDecimal quantity    // Item Quantity
    BigDecimal price       // Total Item Price

    static belongsTo = [invoice: Bill]

    static constraints = {
        description nullable: false
        unitPrice nullable: true
        quantity nullable: true
        price nullable: false
    }

    static mapping = {
        sort dateCreated: 'asc'
    }

    public String getCurrencyType() {
        invoice.currencyType
    }
}

