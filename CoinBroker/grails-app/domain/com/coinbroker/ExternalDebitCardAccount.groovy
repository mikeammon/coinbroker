package com.coinbroker

/**
 * ExternalBankAccount
 * Account we can make electronic transfers to / from using debit card number.
 */
class ExternalDebitCardAccount extends Account implements DebitCardAccountTraits {
    static constraints = {
        monitoring display: false
    }

    public ExternalDebitCardAccount() {
        super()
        // Defaults
        currencyType = Constants.CURRENCY_DOLLAR
        monitoring = MONITOR_EXTERNAL
        mode = MODE_BOTH
        saveCredentials = true
    }
}
