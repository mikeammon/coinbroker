package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Transaction Log
 * Log fund transfers.
 */
class Transaction implements Auditable, Stampable, Ledgered, Notable {
    public final static String STATUS_PENDING   = 'pending'    // Created but not completed
    public final static String STATUS_COMMITTED = 'committed'  // Completed successfully
    public final static String STATUS_FAILED    = 'failed'     // Did nopt complete due to error
    public final static String STATUS_CANCELLED = 'cancelled'  // Cancelled
    public static final def STATUS = [
                STATUS_PENDING, STATUS_COMMITTED, STATUS_FAILED, STATUS_CANCELLED]

    // Add any new fields to CustomerLedger also
    String status
    Date submittedAt
    Date completedAt
    String currencyType    // Currency type
    BigDecimal amount          // Amount transfered
    String withdrawalMemo      // Memo for source account
    String depositMemo // Memo for destination account

    static belongsTo = [
            withdrawalAccount     : Account,
            depositAccount: Account,
    ]
    static hasMany = [balanceEntries: BalanceEntry, bills: Bill]

    static constraints = {
        submittedAt nullable: false
        completedAt nullable: true
        status nullable: false, inList: STATUS, maxSize: 255
        currencyType nullable: false,
                inList: Constants.CURRENCIES, maxSize: 255
        amount nullable: false
        withdrawalMemo nullable: true
        depositMemo nullable: true
        withdrawalAccount nullable: true
        depositAccount nullable: true

    }

    static mapping = {
        sort submittedAt: 'desc'
    }

    public Date getSourceLedger() {
        balanceEntries.find { l -> l.account ==  withdrawalAccount }
    }

    public Date getDestinationLedger() {
        balanceEntries.find { l -> l.account == depositAccount }
    }

    def afterInsert() {
        if (withdrawalAccount.isMonitored) {
            def ledger = new BalanceEntry(account: withdrawalAccount, transaction: this)
            ledger.save()
        }
        if (depositAccount.isMonitored) {
            def ledger = new BalanceEntry(account: depositAccount, transaction: this)
            ledger.save()
        }
    }
}
