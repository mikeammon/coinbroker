package com.coinbroker

import grails.artefact.DomainClass
import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Note
 * Notes attached to objects.
 */
class Note implements Auditable, Stampable {
    // Types
    public final static String COMMENT = 'comment' // Admin comment - Ex: reason for change
    public final static String MESSAGE = 'message' // Note for someone else = Ex: Verify ID
    public final static String ALERT = 'alert'   // Problem that requires action - Ex: Account Expired
    public static final def NOTE_TYPES = [COMMENT, MESSAGE, ALERT]

    // Note Flags - bitmap
    // Clear fields when addressed
    public final static int FLAG_NEEDS_ACTION = 0x00000001
    public final static int FLAG_READ = 0x00000010

    String message // Note text
    String type = COMMENT    // Note type
    int flags   // Flags for required actions
    String domainClass
    Integer domainId

    static constraints = {
        message nullable: false
        type nullable: false, inList: NOTE_TYPES
        flags nullable: true
        domainClass nullable: false
        domainId nullable: false
    }

    static mapping = {
        sort dateCreated: 'desc'
    }

    public boolean hasFlag(int flag) {
        flags & flag
    }

    public boolean setFlag(int flag) {
        if (!hasFlag(flag)) {
            this.flags = this.flags - flag
            this.save()
        }
    }

    public boolean clearFlag(int flag) {
        if (hasFlag(flag)) {
            this.flags = this.flags & ~flag
            this.save()
        }
    }

    def getDomainObject() {
        Class.forName(domainClass).get(domainId)
    }

    def setDomainObject(DomainClass object) {
        domainClass = object.getClass().name
        domainId = object.id
    }

    static def notesFor(Notable notable) {
        Note.findByDomainClassAndDomainId(notable.getClass().name, notable.id)
    }
}
