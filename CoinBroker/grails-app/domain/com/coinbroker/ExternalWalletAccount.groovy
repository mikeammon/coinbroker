package com.coinbroker

/**
 * ExternalBankAccount
 * External Wallet not managed by Coin Broker.
 * Can only push funds to wallet.
 */
class ExternalWalletAccount extends Account implements WalletAccountTraits {
    static constraints = {
        currencyType display: false
        monitoring display: false
        mode display: false
    }

    public ExternalWalletAccount() {
        super()
        // Defaults
        currencyType = Constants.CURRENCY_WELLCOIN
        monitoring  = MONITOR_EXTERNAL
        mode = MODE_DEPOSIT
        saveCredentials = false
    }
}
