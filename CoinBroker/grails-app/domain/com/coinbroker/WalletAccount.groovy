package com.coinbroker

import java.security.SecureRandom

/**
 * WalletAccount
 * Internal Wallet account.
 */
class WalletAccount extends Account implements WalletAccountTraits {
    static constraints = {
        currencyType display: false
        monitoring display: false
        mode display: false
        saveCredentials display: false
    }

    public WalletAccount() {
        super()
        // Defaults
        currencyType = Constants.CURRENCY_WELLCOIN
        monitoring = MONITOR_VIRTUAL
        mode = MODE_BOTH
        saveCredentials = true

//        if (credentials == null) {
//            def realWallet = createWallet()
//            credentials = realWallet.credentials
//        }
    }

    def createWallet() {
        // TODO: neet to call wallet API here
        def credentials = new WalletCredentials(
                publicKey: randomKey(),
                privateKey: randomKey(),
        )
        [ credentials: credentials ]
    }

    static transients = ['random']
    SecureRandom random = new SecureRandom();
    def randomKey() {
        def bytes = new byte[20]
        random.nextBytes(bytes)
    }
}
