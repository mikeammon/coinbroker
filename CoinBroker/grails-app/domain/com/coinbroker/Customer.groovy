package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Customer
 * Customer Account
 * Account is created with state = enabled
 * KYC/AML requirements are checked by app before customer is created
 */
class Customer implements Auditable, Stampable, Ledgered, Notable {
    public final static String TYPE_INDIVIDUAL = 'individual'
    public final static String TYPE_BUSINESS   = 'business'

    // Add any new fields to CustomerLedger also
    String type = TYPE_INDIVIDUAL  // individual, business
    String status = Constants.STATUS_ENABLED // pending, enabled, disabled
    String accountingCurrency = Constants.CURRENCY_DOLLAR
    String kycAmlId
    Boolean isVendor = false
    Boolean isAppUser = false

    // Contact Info
    PersonName contactName   // Individual or business contact
    String businessName      // Business name - Optional for personal account
    Address billingAddress
    String phoneNumber
    String email

    Integer appUsageLevel

    static hasMany = [
            accounts: Account,
            creditApplications: CreditApplication,
            payableBills: Bill,
            receivableBills: Bill,
    ]

    static mappedBy = [
            payableBills: 'customer',
            receivableBills: 'vendor',
    ]

    static embedded = ['contactName', 'billingAddress']

    static mapping = {
        isVendor  sqlType: "tinyint(1)"
        isAppUser sqlType: "tinyint(1)"
    }

    static constraints = {
        type nullable: false, inList: [TYPE_INDIVIDUAL, TYPE_BUSINESS], maxSize: 255
        contactName nullable: false
        businessName nullable: true
        status nullable: false, inList: Constants.DOMAIN_STATUS, maxSize: 255
        accountingCurrency nullable: false
        kycAmlId nullable: false
        isVendor nullable: false
        isAppUser nullable: false
        billingAddress nullable: false
        phoneNumber nullable: false
        email nullable: false, email: true
        appUsageLevel nullable: true
    }

    public boolean getEnabled() {
        status == Constants.STATUS_ENABLED
    }

    public String getName() {
        type == TYPE_INDIVIDUAL ? contactName.toString() : businessName
    }
}
