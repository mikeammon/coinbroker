package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Account
 * Base class for a source or destination of transfers.
 */
abstract class Account implements Auditable, Stampable, Notable {
    // Access Modes - bitmap
    public final static int MODE_WITHDRAW = 0x0001
    public final static int MODE_DEPOSIT  = 0x0002
    public final static int MODE_BOTH = MODE_WITHDRAW | MODE_DEPOSIT
    public static final def ACCOUNT_MODES = [MODE_WITHDRAW, MODE_DEPOSIT, MODE_BOTH]

    // Locations
    public final static String MONITOR_VIRTUAL = 'virtual'
    public final static String MONITOR_INTERNAL = 'internal'
    public final static String MONITOR_EXTERNAL = 'external'
    public static final def MONITOR_MODES = [MONITOR_VIRTUAL, MONITOR_INTERNAL, MONITOR_EXTERNAL]

    String status = Constants.STATUS_ENABLED                    // Ex: enables, disables
    String currencyType              // Currency of account
    String monitoring = MONITOR_INTERNAL                // Virtual, Internal or External
    Integer mode = MODE_BOTH                     // Access mode - withdraw, deposit
    String name                      // Display name
    Boolean saveCredentials = false  // Save credentials after transaction completes

    static belongsTo = [customer: Customer]
    static hasMany = [
            withdrawTransactions: Transaction,
            depositTransactions: Transaction,
            buyTokenLots: BuyTokenLot,
            sellTokenLots: SellTokenLot,
            balanceEntries: BalanceEntry,
    ]

    static mappedBy = [
            withdrawTransactions: 'withdrawalAccount',
            depositTransactions: 'depositAccount',
            buyTokenLots: 'buyAccount',
            sellTokenLots: 'sellAccount',
    ]

    static mapping = {
        saveCredentials sqlType: "tinyint(1)"
    }

    static constraints = {
        name nullable: false
        status nullable: false, inList: Constants.DOMAIN_STATUS, maxSize: 255
        currencyType nullable: false, inList: Constants.CURRENCIES, maxSize: 255
        monitoring nullable: false, inList: MONITOR_MODES, maxSize: 255
        mode nullable: false, inList: ACCOUNT_MODES, maxSize: 255
        saveCredentials nullable: false
    }

    public boolean getEnabled() {
        status == Constants.STATUS_ENABLED
    }

    public boolean getCanWithdraw() {
        mode & MODE_WITHDRAW
    }

    public boolean getCanDeposit() {
        mode & MODE_DEPOSIT
    }

    public boolean isMonitored() {
        monitoring == MONITOR_VIRTUAL
    }

     public boolean isInternal() {
        monitoring == MONITOR_INTERNAL
    }

    public boolean isExternal() {
        monitoring == MONITOR_EXTERNAL
    }

    public getLedgers() {
        accountLedgers.sort { a, b -> b.submittedAt <=> a.submittedAt }
    }

    public BigDecimal getbalance() {
        ledgers.first?.accountBalance
    }

    // TODO: make abstract
    public Credentials getCredentials() {}

}
