package com.coinbroker

/**
 * ExternalBankAccount
 * Account we can make electronic transfers to / from using routing number and account number.
 */
class ExternalBankAccount extends Account implements BankAccountTraits {
    static constraints = {
        monitoring display: false
    }

    public ExternalBankAccount() {
        super()
        // Defaults
        currencyType = Constants.CURRENCY_WELLCOIN
        monitoring = MONITOR_EXTERNAL
        mode = MODE_BOTH
        saveCredentials = true
    }
}
