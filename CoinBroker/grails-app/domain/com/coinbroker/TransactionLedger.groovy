package com.coinbroker

class TransactionLedger implements Ledger {
    // Include all fields from Transaction
    String status
    Date submittedAt
    Date completedAt
    String currencyType
    BigDecimal amount
    String withdrawalMemo
    String depositMemo

    static constraints = {
        submittedAt nullable: true
        completedAt nullable: true
        status nullable: true
        currencyType nullable: true
        amount nullable: true
        withdrawalMemo nullable: true
        depositMemo nullable: true
        withdrawalAccount nullable: true
        depositAccount nullable: true

        ledgeredId nullable: true
        dateCreated nullable: true
        lastUpdated nullable: true
        createdBy nullable: true
        lastUpdatedBy nullable: true
    }
}
