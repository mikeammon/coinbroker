package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Bill
 * Details for transaction items
 */
class Bill implements Auditable, Stampable, Notable {
    String status
    String vendorBillId  // ID assigned by vendor
    Date billingDate     // Date bill created, set by vendor
    Date dueDate         // Due date set by vendor, null for on demand charges
    String description   // Description
    BigDecimal amountDue // Total invoice amount

    static belongsTo = [vendor: Customer, customer: Customer, transaction: Transaction]
    static hasMany = [billItems: BillItem]

    static constraints = {
        status nullable: false
        vendorBillId nullable: false
        billingDate nullable: false
        dueDate nullable: true
        description nullable: false
        amountDue nullable: false
    }

    static mapping = {
        sort dateCreated: 'asc'
    }

    public String getCurrencyType() {
        transaction.currencyType
    }
}
