package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * TokenLot
 * Track token lots for taxes.
 * Buy Token Lots have basis price.
 * Sell Token lots have sale price.
 * Sell lots are assigned to buy lots in first in first out order.
 * Price can be set to:
 *   Market Price at time of transaction
 *   Set based on Exchange charges
 *   Reported by customer
 * For tax purposes, all transfers are treated as a buy or sell.
 */
class TokenLot implements Auditable, Stampable, Notable {
    public final static String ACTION_BUY = 'buy'
    public final static String ACTION_SELL = 'sell'

    public final static String PRICE_EXCHANGE = 'exchange'
    public final static String PRICE_MARKET = 'market'
    public final static String PRICE_CUSTOMER = 'customer'

    String action       // Buy or sell
    BigDecimal unitPrice    // Unit price - basis or sale price
    BigDecimal quantity     // Number of tokens in lot
    String priceSource  // How price was set

    static belongsTo = [transaction: Transaction]

    static constraints = {
        action nullable: false, inList: [ACTION_BUY, ACTION_SELL]
        unitPrice nullable: true
        quantity nullable: false
        priceSource nullable: false, inList: [PRICE_EXCHANGE, PRICE_MARKET, PRICE_CUSTOMER]
    }

    static mapping = {
        sort dateCreated: 'asc'
    }

    public String getCurrencyType() {
        account.currencyType
    }
}
