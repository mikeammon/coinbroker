package com.coinbroker

/**
 * ExternalCreditCardAccount
 * Account we can make electronic transfers to / from a credit card
 */
class ExternalCreditCardAccount extends Account implements CreditCardAccountTraits {
    static constraints = {
        monitoring display: false
    }

    public ExternalCreditCardAccount() {
        super()
        // Defaults
        currencyType = Constants.CURRENCY_DOLLAR
        monitoring = MONITOR_EXTERNAL
        mode = MODE_BOTH
        saveCredentials = true
    }
}
