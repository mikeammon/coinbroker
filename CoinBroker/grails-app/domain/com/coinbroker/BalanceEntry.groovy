package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * BalanceEntry
 * Links global transactions list to Accounts.
 * Ledger has the running accountBalance.
 */
class BalanceEntry implements Auditable, Stampable, Notable {
    public final static String WITHDRAWAL = 'withdrawal'
    public final static String DEPOSIT = 'deposit'
    public static final def DEPOSIT_OR_WITHDRAWAL = [WITHDRAWAL, DEPOSIT]

    BigDecimal accountBalance // Balance excluding pending transactions
    BigDecimal pendingDeposits // Balance after transaction
    BigDecimal pendingWithdrawals // Balance after transaction
    String depositOrWithdrawal    // Source or destination account in transaction

    static belongsTo = [account: Account, transaction: Transaction]

    static constraints = {
        accountBalance nullable: false
        pendingDeposits nullable: false
        pendingWithdrawals nullable: false
        depositOrWithdrawal nullable: false, inList: DEPOSIT_OR_WITHDRAWAL, maxSize: 255
    }

    public boolean getIsWithdrawal() {
        depositOrWithdrawal == WITHDRAWAL
    }

    public boolean getIsDeposit() {
        depositOrWithdrawal == DEPOSIT
    }

    public BigDecimal getAmount() {
        (isWithdrawal ? (-transaction?.amount) : transaction?.amount) ?: 0
    }

    public BigDecimal getMemo() {
        isWithdrawal ? transaction?.withdrawalMemo : transaction?.depositMemo
    }

    public Date getTimestamp() {
        transaction?.submittedAt
    }

    public BalanceEntry getPrevious() {
        if (timestamp == null || account == null) { return null }
        account.balanceEntries.find { l -> (l.submittedAt < timestamp) || ((l.submittedAt == timestamp) && (l.id < id)) }

    }

    public BalanceEntry getNext() {
        if (timestamp == null || account == null) { return null }
        account.balanceEntries.reverse().find { l -> (l.submittedAt > timestamp) || ((l.submittedAt == timestamp) && (l.id > id)) }
    }

    public void setBalances() {
        pendingDeposits    = previous?.accountBalance ?: 0
        pendingWithdrawals = previous?.accountBalance ?: 0
        accountBalance     = previous?.accountBalance ?: 0
        if (transaction) {
            if (transaction.status == Transaction.STATUS_COMMITTED) {
                accountBalance += amount
            } else if (transaction.status == Transaction.STATUS_PENDING) {
                if (isDeposit) {
                    pendingDeposits += amount
                } else if (isWithdrawal) {
                    pendingWithdrawals += amount
                }
            }
        }
        if (next) {
            next.setBalances()
            next.save()
        }
    }

    public BigDecimal getAvalableBalance() {
        accountBalance + pendingWithdrawals
    }

    public BigDecimal getPendingBalance() {
        accountBalance + pendingDeposits + pendingWithdrawals
    }

    public void setDepositOrWithdrawal() {
        this.depositOrWithdrawal = (tranaction.withdrawalAccount == this) ? WITHDRAWAL : DEPOSIT
    }

    def beforeInsert() {
        if (depositOrWithdrawal == null) {
            setDepositOrWithdrawal()
        }
        if (accountBalance == null) {
            setBalances()
        }
        true
    }

}
