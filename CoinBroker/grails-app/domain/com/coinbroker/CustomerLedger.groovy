package com.coinbroker

import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

/**
 * Customer
 * Customer Account
 * Account is created with state = enabled
 * KYC/AML requirements are checked by app before customer is created
 */
class CustomerLedger implements Ledger {
    // Include all fields from Customer
    String type
    String status
    String accountingCurrency
    String kycAmlId
    Boolean isVendor
    Boolean isAppUser
    PersonName contactName
    String businessName
    Address billingAddress
    String phoneNumber
    String email
    Integer appUsageLevel

    static embedded = ['contactName', 'billingAddress']

    static mapping = {
        isVendor  sqlType: "tinyint(1)"
        isAppUser sqlType: "tinyint(1)"
    }

    static constraints = {
        type nullable: true
        contactName nullable: true
        businessName nullable: true
        status nullable: true
        accountingCurrency nullable: true
        kycAmlId nullable: true
        isVendor nullable: true
        isAppUser nullable: true
        billingAddress nullable: true
        phoneNumber nullable: true
        email nullable: true
        appUsageLevel nullable: true

        ledgeredId nullable: true
        dateCreated nullable: true
        lastUpdated nullable: true
        createdBy nullable: true
        lastUpdatedBy nullable: true
    }
}
