package com.coinbroker

import com.bloomhealthco.jasypt.GormEncryptedStringType
import grails.plugins.orm.auditable.Auditable
import grails.plugins.orm.auditable.Stampable

class CreditApplication implements Auditable, Stampable, Notable {
    Date dob // Date of birth
    String ssn // Social Security Number | Taxpayer ID number
    Boolean creditCheckAuthorization // Customer aggreedit to credit check
    Integer appUsageLevel // Usage level from Wellsa App

    // Housing
    BigDecimal monthlyHousingCost // Customer reported housing cost
    Boolean ownsHome // Customer opwns home

    // Address
    Address currentAddress // Current address

    // Optional inputs
    Address previousAddress // Previous address
    DriversLicense driversLicence // Driver's License

    static belongsTo = [customer: Customer]

    static embedded = ['currentAddress', 'previousAddress', 'driversLicence']

    static mapping = {
        creditCheckAuthorization  sqlType: "tinyint(1)"
        ownsHome sqlType: "tinyint(1)"
        ssn  type: GormEncryptedStringType
    }

    static constraints = {
        dob nullable: false
        ssn nullable: true, password: true
        creditCheckAuthorization nullable: false
        appUsageLevel nullable: true
        monthlyHousingCost nullable: false
        ownsHome nullable: false
        currentAddress nullable: false
        previousAddress nullable: true
        driversLicence nullable: true
    }
}
