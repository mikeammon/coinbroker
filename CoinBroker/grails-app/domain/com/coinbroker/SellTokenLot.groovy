package com.coinbroker

/**
 * SellTokenLot
 * Lot of tokens from a sell or transfer from wallet
 */
class SellTokenLot extends TokenLot {
    static belongsTo = [sourceLot: BuyTokenLot, sellAccount: Account]

    public SellTokenLot() {
        super()
        // Defaults
        action = ACTION_SELL
    }
}
