package com.coinbroker

import grails.gorm.services.Service

@Service(ExternalBankAccount)
interface ExternalBankAccountService {

    ExternalBankAccount get(Serializable id)

    List<ExternalBankAccount> list(Map args)

    Long count()

    void delete(Serializable id)

    ExternalBankAccount save(ExternalBankAccount externalBankAccount)

}