package com.coinbroker

import grails.gorm.services.Service

@Service(BuyTokenLot)
interface BuyTokenLotService {

    BuyTokenLot get(Serializable id)

    List<BuyTokenLot> list(Map args)

    Long count()

    void delete(Serializable id)

    BuyTokenLot save(BuyTokenLot buyTokenLot)

}