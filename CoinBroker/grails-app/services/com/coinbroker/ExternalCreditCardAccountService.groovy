package com.coinbroker

import grails.gorm.services.Service

@Service(ExternalCreditCardAccount)
interface ExternalCreditCardAccountService {

    ExternalCreditCardAccount get(Serializable id)

    List<ExternalCreditCardAccount> list(Map args)

    Long count()

    void delete(Serializable id)

    ExternalCreditCardAccount save(ExternalCreditCardAccount externalCreditCardAccount)

}