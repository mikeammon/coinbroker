package com.coinbroker

import grails.gorm.services.Service

@Service(ExternalWalletAccount)
interface ExternalWalletAccountService {

    ExternalWalletAccount get(Serializable id)

    List<ExternalWalletAccount> list(Map args)

    Long count()

    void delete(Serializable id)

    ExternalWalletAccount save(ExternalWalletAccount externalWalletAccount)

}