package com.coinbroker

import grails.gorm.services.Service

@Service(WalletAccount)
interface WalletAccountService {

    WalletAccount get(Serializable id)

    List<WalletAccount> list(Map args)

    Long count()

    void delete(Serializable id)

    WalletAccount save(WalletAccount walletAccount)

}