package com.coinbroker

import grails.gorm.services.Service

@Service(BillItem)
interface BillItemService {

    BillItem get(Serializable id)

    List<BillItem> list(Map args)

    Long count()

    void delete(Serializable id)

    BillItem save(BillItem invoiceItem)

}