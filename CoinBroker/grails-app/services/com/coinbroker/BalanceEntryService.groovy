package com.coinbroker

import grails.gorm.services.Service

@Service(BalanceEntry)
interface BalanceEntryService {

    BalanceEntry get(Serializable id)

    List<BalanceEntry> list(Map args)

    Long count()

    void delete(Serializable id)

    BalanceEntry save(BalanceEntry balanceEntry)

}