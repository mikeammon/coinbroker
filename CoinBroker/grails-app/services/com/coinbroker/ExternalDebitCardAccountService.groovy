package com.coinbroker

import grails.gorm.services.Service

@Service(ExternalDebitCardAccount)
interface ExternalDebitCardAccountService {

    ExternalDebitCardAccount get(Serializable id)

    List<ExternalDebitCardAccount> list(Map args)

    Long count()

    void delete(Serializable id)

    ExternalDebitCardAccount save(ExternalDebitCardAccount externalDebitCardAccount)

}