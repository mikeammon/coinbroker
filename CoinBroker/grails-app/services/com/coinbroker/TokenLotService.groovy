package com.coinbroker

import grails.gorm.services.Service

@Service(TokenLot)
interface TokenLotService {

    TokenLot get(Serializable id)

    List<TokenLot> list(Map args)

    Long count()

    void delete(Serializable id)

    TokenLot save(TokenLot tokenLot)

}