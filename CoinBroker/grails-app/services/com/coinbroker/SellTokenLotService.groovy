package com.coinbroker

import grails.gorm.services.Service

@Service(SellTokenLot)
interface SellTokenLotService {

    SellTokenLot get(Serializable id)

    List<SellTokenLot> list(Map args)

    Long count()

    void delete(Serializable id)

    SellTokenLot save(SellTokenLot sellTokenLot)

}