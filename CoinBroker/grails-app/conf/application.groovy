// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.coinbroker.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.coinbroker.UserRole'
grails.plugin.springsecurity.authority.className = 'com.coinbroker.Role'
grails.plugin.springsecurity.authority.groupAuthorityNameField = 'authorities'
grails.plugin.springsecurity.useRoleGroups = true
grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.useX509 = true
grails.plugin.springsecurity.x509.subjectDnRegex = "CN=(.*?)(?:,|\$|/)"
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
//        [pattern: '/**',      access: 'isAuthenticated()'],
        [pattern: '/**', access: ['permitAll']],
//        [pattern: '/appUsageLevel/**',      access: 'isAuthenticated()'],
//        [pattern: '/billing/**',      access: 'isAuthenticated()'],
//        [pattern: '/coinAccount/**',      access: 'isAuthenticated()'],
//        [pattern: '/creditAccount/**',      access: 'isAuthenticated()'],
//        [pattern: '/exchange/**',      access: 'isAuthenticated()'],
//        [pattern: '/externalAccount/**',      access: 'isAuthenticated()'],
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

// Added by the Audit-Logging plugin:
grails.plugin.auditLog.auditDomainClassName = 'com.coinbroker.AuditTrail'

