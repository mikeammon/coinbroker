package com.coinbroker

import com.bloomhealthco.jasypt.GormEncryptedStringType

class DebitCardCredentials extends CreditCardCredentials {
    String pin

    static constraints = {
        pin nullable: true, password: true
    }

    static mapping = {
        pin  type: GormEncryptedStringType
    }

    public void clearKey() {
        super.clearKey()
        pin = null
    }
}