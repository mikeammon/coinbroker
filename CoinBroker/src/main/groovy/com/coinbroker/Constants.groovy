package com.coinbroker

class Constants {
    // Domain Object States
    public final static String STATUS_PENDING = 'pending'     // Created but not enabled yet
    public final static String STATUS_ENABLED = 'enabled'     // Active
    public final static String STATUS_DISABLED = 'disabled'    // Disabled
    public final static String STATUS_BLOCKED = 'blocked'     // Temporarily unavaiable - need attention
    public final static String STATUS_DONT_PERSIST = 'dontPersist' // Don't save credentials after use
    public static final def DOMAIN_STATUS = [
            STATUS_PENDING, STATUS_ENABLED, STATUS_DISABLED, STATUS_BLOCKED, STATUS_DONT_PERSIST]

    // Currencies Supported
    public final static String CURRENCY_DOLLAR = 'USD'
    public final static String CURRENCY_WELLCOIN = 'WCOIN'
    public final static String CURRENCY_BITCOIN = 'BTC'
    public final static String CURRENCY_ETHER = 'ETH'
    public final static String CURRENCY_XRP = 'XRP'
    public static final def CURRENCIES = [CURRENCY_DOLLAR, CURRENCY_WELLCOIN, CURRENCY_BITCOIN, CURRENCY_ETHER, CURRENCY_XRP]
}
