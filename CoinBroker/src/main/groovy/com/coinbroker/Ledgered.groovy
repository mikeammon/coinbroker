package com.coinbroker

import org.grails.datastore.mapping.model.PersistentEntity
/*
 * Ledgered
 * Support for a ledger table backing up a main class
 * Adds a snapshot row to the ledger table when a main class is crated or updated
 *
 * To use:
 * Make a Ledger class named with the main class + "Ledger"
 *   Ex: CustomerLedger is the ledger class for Customer
 * In the Ledger Clas:s
 *   Add implements Ledger
 *   Add all the fields defined in the main class
 *   Copy static embedded, mapping, constraints from main class
 *   Change constraints to be "nullable: true"
 *   Add to constraints
 *     ledgeredId nullable: true
 *     dateCreated nullable: true
 *     lastUpdated nullable: true
 *     createdBy nullable: true
 *     lastUpdatedBy nullable: true
 *  In main class:
 *    Add "implements Ledgered"
 */
trait Ledgered {
    public static String LEDGER_SUFFIX = 'Ledger'

    def afterInsert() {
        createLedger()
        true
    }

    def afterUpdate() {
        createLedger()
        true
    }

    def getLedgers() {
        ledgerClass.findByLedgeredId(this.getId())
    }

    def getLedgerClass() {
        def ledgerClassName = this.getClass().name + LEDGER_SUFFIX
        Class.forName(ledgerClassName)
    }

    def createLedger() {
        def ledger = ledgerClass.newInstance()
        ledger.properties = properties
        ledger.ledgeredId = this.getId()
        ledger.save()
    }
}