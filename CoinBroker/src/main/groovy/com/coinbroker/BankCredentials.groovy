package com.coinbroker

import com.bloomhealthco.jasypt.GormEncryptedStringType

class BankCredentials implements Credentials {
    String bankName
    String accountNumber
    String routingNumber

    static constraints = {
        bankName nullable: false
        accountNumber nullable: false
        routingNumber nullable: false
    }

    static mapping = {
        bankName  type: GormEncryptedStringType
        accountNumber type: GormEncryptedStringType
        routingNumber type: GormEncryptedStringType
    }

    public void clearKey() {
        // no fields are keys
    }
}