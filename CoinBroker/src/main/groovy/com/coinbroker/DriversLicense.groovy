package com.coinbroker

/**
 * Drivers License
 */
class DriversLicense {
    String number
    String state

    static constraints = {
        number nullable: false
        state nullable: false
    }
}
