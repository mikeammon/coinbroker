package com.coinbroker

/**
 * PersonName
 */
class PersonName {
    String first
    String middle
    String last

    static constraints = {
        first nullable: false
        middle nullable: true
        last nullable: false
    }

    public String toString() {
        [first, middle, last].join(' ')
    }
}
