package com.coinbroker

trait WalletAccountTraits {
    WalletCredentials walletCredentials

    static embedded = ['walletCredentials']

    static constraints = {
        walletCredentials nullable: false
    }

    Credentials getCredentials() {
        walletCredentials
    }
}