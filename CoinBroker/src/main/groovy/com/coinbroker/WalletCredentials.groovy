package com.coinbroker

import com.bloomhealthco.jasypt.GormEncryptedStringType

class WalletCredentials implements Credentials {
    String publicKey
    String privateKey

    static constraints = {
        publicKey nullable: false
        privateKey nullable: true, password: true
    }

    static mapping = {
        publicKey  type: GormEncryptedStringType
        privateKey type: GormEncryptedStringType
    }

    public void clearKey() {
        privateKey = null
    }
}