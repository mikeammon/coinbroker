package com.coinbroker

trait CreditCardAccountTraits {
    CreditCardCredentials creditCardCredentials

    static embedded = ['creditCardCredentials']

    static constraints = {
        creditCardCredentials nullable: false
    }

    Credentials getCredentials() {
        creditCardCredentials
    }
}