package com.coinbroker

trait DebitCardAccountTraits {
    DebitCardCredentials debitCardCredentials

    static embedded = ['debitCardCredentials']

    static constraints = {
        debitCardCredentials nullable: false
    }

    Credentials getCredentials() {
        debitCardCredentials
    }
}