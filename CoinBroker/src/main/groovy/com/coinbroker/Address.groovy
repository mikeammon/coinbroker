package com.coinbroker

/**
 * Address
 */
class Address {
    String address1
    String address2
    String city
    String state
    String postalCode
    String country

    static constraints = {
        address1 nullable: false
        address2 nullable: true
        city nullable: false
        state nullable: false
        postalCode nullable: false
        country nullable: false
    }
}
