package com.coinbroker

import org.grails.datastore.gorm.GormEntity

/*
 * Ledger
 * Leders table for saving snapshots of a main class
 * See Ledgered for usage notes
 */
trait Ledger {
    Date dateCreated
    Date lastUpdated
    String createdBy
    String lastUpdatedBy
    Integer ledgeredId

    // Write once - prevent updates
    def beforeUpdate() {
        false
    }
}