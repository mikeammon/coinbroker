package com.coinbroker

import com.bloomhealthco.jasypt.GormEncryptedStringType

class CreditCardCredentials implements Credentials {
    String nameOnCard
    String cardNumber
    String securityCode
    String expirationYear
    String expirationMonth

    static constraints = {
        nameOnCard nullable: false
        cardNumber nullable: false
        securityCode nullable: true, password: true
        expirationYear nullable: false
        expirationMonth nullable: false
    }

    static mapping = {
        nameOnCard  type: GormEncryptedStringType
        cardNumber type: GormEncryptedStringType
        securityCode type: GormEncryptedStringType
        expirationYear type: GormEncryptedStringType
        expirationMonth type: GormEncryptedStringType
    }

    public void clearKey() {
        securityCode = null
    }
}