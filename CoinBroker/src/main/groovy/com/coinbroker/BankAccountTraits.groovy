package com.coinbroker

trait BankAccountTraits {
    BankCredentials bankCredentials

    static embedded = ['bankCredentials']

    static constraints = {
        bankCredentials nullable: false
    }

    Credentials getCredentials() {
        bankCredentials
    }
}