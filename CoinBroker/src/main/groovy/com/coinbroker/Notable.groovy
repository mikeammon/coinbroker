package com.coinbroker

trait Notable {
    def getNotes() {
        Note.notesFor(this)
    }

    def addNote(params) {
        Note note = new Note(params)
        note.DomainObject = this
        note.save()
    }
}