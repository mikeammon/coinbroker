package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class SellTokenLotServiceSpec extends Specification {

    SellTokenLotService sellTokenLotService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new SellTokenLot(...).save(flush: true, failOnError: true)
        //new SellTokenLot(...).save(flush: true, failOnError: true)
        //SellTokenLot sellTokenLot = new SellTokenLot(...).save(flush: true, failOnError: true)
        //new SellTokenLot(...).save(flush: true, failOnError: true)
        //new SellTokenLot(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //sellTokenLot.id
    }

    void "test get"() {
        setupData()

        expect:
        sellTokenLotService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<SellTokenLot> sellTokenLotList = sellTokenLotService.list(max: 2, offset: 2)

        then:
        sellTokenLotList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        sellTokenLotService.count() == 5
    }

    void "test delete"() {
        Long sellTokenLotId = setupData()

        expect:
        sellTokenLotService.count() == 5

        when:
        sellTokenLotService.delete(sellTokenLotId)
        sessionFactory.currentSession.flush()

        then:
        sellTokenLotService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        SellTokenLot sellTokenLot = new SellTokenLot()
        sellTokenLotService.save(sellTokenLot)

        then:
        sellTokenLot.id != null
    }
}
