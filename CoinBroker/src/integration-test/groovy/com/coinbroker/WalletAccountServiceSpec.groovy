package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class WalletAccountServiceSpec extends Specification {

    WalletAccountService walletAccountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new WalletAccount(...).save(flush: true, failOnError: true)
        //new WalletAccount(...).save(flush: true, failOnError: true)
        //WalletAccount walletAccount = new WalletAccount(...).save(flush: true, failOnError: true)
        //new WalletAccount(...).save(flush: true, failOnError: true)
        //new WalletAccount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //walletAccount.id
    }

    void "test get"() {
        setupData()

        expect:
        walletAccountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<WalletAccount> walletAccountList = walletAccountService.list(max: 2, offset: 2)

        then:
        walletAccountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        walletAccountService.count() == 5
    }

    void "test delete"() {
        Long walletAccountId = setupData()

        expect:
        walletAccountService.count() == 5

        when:
        walletAccountService.delete(walletAccountId)
        sessionFactory.currentSession.flush()

        then:
        walletAccountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        WalletAccount walletAccount = new WalletAccount()
        walletAccountService.save(walletAccount)

        then:
        walletAccount.id != null
    }
}
