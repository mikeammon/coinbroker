package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ExternalBankAccountServiceSpec extends Specification {

    ExternalBankAccountService externalBankAccountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ExternalBankAccount(...).save(flush: true, failOnError: true)
        //new ExternalBankAccount(...).save(flush: true, failOnError: true)
        //ExternalBankAccount externalBankAccount = new ExternalBankAccount(...).save(flush: true, failOnError: true)
        //new ExternalBankAccount(...).save(flush: true, failOnError: true)
        //new ExternalBankAccount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //externalBankAccount.id
    }

    void "test get"() {
        setupData()

        expect:
        externalBankAccountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ExternalBankAccount> externalBankAccountList = externalBankAccountService.list(max: 2, offset: 2)

        then:
        externalBankAccountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        externalBankAccountService.count() == 5
    }

    void "test delete"() {
        Long externalBankAccountId = setupData()

        expect:
        externalBankAccountService.count() == 5

        when:
        externalBankAccountService.delete(externalBankAccountId)
        sessionFactory.currentSession.flush()

        then:
        externalBankAccountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ExternalBankAccount externalBankAccount = new ExternalBankAccount()
        externalBankAccountService.save(externalBankAccount)

        then:
        externalBankAccount.id != null
    }
}
