package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BillItemServiceSpec extends Specification {

    BillItemService invoiceItemService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BillItem(...).save(flush: true, failOnError: true)
        //new BillItem(...).save(flush: true, failOnError: true)
        //BillItem invoiceItem = new BillItem(...).save(flush: true, failOnError: true)
        //new BillItem(...).save(flush: true, failOnError: true)
        //new BillItem(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //invoiceItem.id
    }

    void "test get"() {
        setupData()

        expect:
        invoiceItemService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BillItem> invoiceItemList = invoiceItemService.list(max: 2, offset: 2)

        then:
        invoiceItemList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        invoiceItemService.count() == 5
    }

    void "test delete"() {
        Long invoiceItemId = setupData()

        expect:
        invoiceItemService.count() == 5

        when:
        invoiceItemService.delete(invoiceItemId)
        sessionFactory.currentSession.flush()

        then:
        invoiceItemService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BillItem invoiceItem = new BillItem()
        invoiceItemService.save(invoiceItem)

        then:
        invoiceItem.id != null
    }
}
