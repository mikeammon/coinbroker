package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ExternalCreditCardAccountServiceSpec extends Specification {

    ExternalCreditCardAccountService externalCreditCardAccountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ExternalCreditCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalCreditCardAccount(...).save(flush: true, failOnError: true)
        //ExternalCreditCardAccount externalCreditCardAccount = new ExternalCreditCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalCreditCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalCreditCardAccount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //externalCreditCardAccount.id
    }

    void "test get"() {
        setupData()

        expect:
        externalCreditCardAccountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ExternalCreditCardAccount> externalCreditCardAccountList = externalCreditCardAccountService.list(max: 2, offset: 2)

        then:
        externalCreditCardAccountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        externalCreditCardAccountService.count() == 5
    }

    void "test delete"() {
        Long externalCreditCardAccountId = setupData()

        expect:
        externalCreditCardAccountService.count() == 5

        when:
        externalCreditCardAccountService.delete(externalCreditCardAccountId)
        sessionFactory.currentSession.flush()

        then:
        externalCreditCardAccountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ExternalCreditCardAccount externalCreditCardAccount = new ExternalCreditCardAccount()
        externalCreditCardAccountService.save(externalCreditCardAccount)

        then:
        externalCreditCardAccount.id != null
    }
}
