package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BalanceEntryServiceSpec extends Specification {

    BalanceEntryService balanceEntryService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BalanceEntry(...).save(flush: true, failOnError: true)
        //new BalanceEntry(...).save(flush: true, failOnError: true)
        //BalanceEntry balanceEntry = new BalanceEntry(...).save(flush: true, failOnError: true)
        //new BalanceEntry(...).save(flush: true, failOnError: true)
        //new BalanceEntry(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //balanceEntry.id
    }

    void "test get"() {
        setupData()

        expect:
        balanceEntryService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BalanceEntry> balanceEntryList = balanceEntryService.list(max: 2, offset: 2)

        then:
        balanceEntryList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        balanceEntryService.count() == 5
    }

    void "test delete"() {
        Long balanceEntryId = setupData()

        expect:
        balanceEntryService.count() == 5

        when:
        balanceEntryService.delete(balanceEntryId)
        sessionFactory.currentSession.flush()

        then:
        balanceEntryService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BalanceEntry balanceEntry = new BalanceEntry()
        balanceEntryService.save(balanceEntry)

        then:
        balanceEntry.id != null
    }
}
