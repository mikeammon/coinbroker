package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BuyTokenLotServiceSpec extends Specification {

    BuyTokenLotService buyTokenLotService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BuyTokenLot(...).save(flush: true, failOnError: true)
        //new BuyTokenLot(...).save(flush: true, failOnError: true)
        //BuyTokenLot buyTokenLot = new BuyTokenLot(...).save(flush: true, failOnError: true)
        //new BuyTokenLot(...).save(flush: true, failOnError: true)
        //new BuyTokenLot(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //buyTokenLot.id
    }

    void "test get"() {
        setupData()

        expect:
        buyTokenLotService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BuyTokenLot> buyTokenLotList = buyTokenLotService.list(max: 2, offset: 2)

        then:
        buyTokenLotList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        buyTokenLotService.count() == 5
    }

    void "test delete"() {
        Long buyTokenLotId = setupData()

        expect:
        buyTokenLotService.count() == 5

        when:
        buyTokenLotService.delete(buyTokenLotId)
        sessionFactory.currentSession.flush()

        then:
        buyTokenLotService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BuyTokenLot buyTokenLot = new BuyTokenLot()
        buyTokenLotService.save(buyTokenLot)

        then:
        buyTokenLot.id != null
    }
}
