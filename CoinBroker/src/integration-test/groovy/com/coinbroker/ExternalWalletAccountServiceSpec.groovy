package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ExternalWalletAccountServiceSpec extends Specification {

    ExternalWalletAccountService externalWalletAccountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ExternalWalletAccount(...).save(flush: true, failOnError: true)
        //new ExternalWalletAccount(...).save(flush: true, failOnError: true)
        //ExternalWalletAccount externalWalletAccount = new ExternalWalletAccount(...).save(flush: true, failOnError: true)
        //new ExternalWalletAccount(...).save(flush: true, failOnError: true)
        //new ExternalWalletAccount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //externalWalletAccount.id
    }

    void "test get"() {
        setupData()

        expect:
        externalWalletAccountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ExternalWalletAccount> externalWalletAccountList = externalWalletAccountService.list(max: 2, offset: 2)

        then:
        externalWalletAccountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        externalWalletAccountService.count() == 5
    }

    void "test delete"() {
        Long externalWalletAccountId = setupData()

        expect:
        externalWalletAccountService.count() == 5

        when:
        externalWalletAccountService.delete(externalWalletAccountId)
        sessionFactory.currentSession.flush()

        then:
        externalWalletAccountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ExternalWalletAccount externalWalletAccount = new ExternalWalletAccount()
        externalWalletAccountService.save(externalWalletAccount)

        then:
        externalWalletAccount.id != null
    }
}
