package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ExternalDebitCardAccountServiceSpec extends Specification {

    ExternalDebitCardAccountService externalDebitCardAccountService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ExternalDebitCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalDebitCardAccount(...).save(flush: true, failOnError: true)
        //ExternalDebitCardAccount externalDebitCardAccount = new ExternalDebitCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalDebitCardAccount(...).save(flush: true, failOnError: true)
        //new ExternalDebitCardAccount(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //externalDebitCardAccount.id
    }

    void "test get"() {
        setupData()

        expect:
        externalDebitCardAccountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ExternalDebitCardAccount> externalDebitCardAccountList = externalDebitCardAccountService.list(max: 2, offset: 2)

        then:
        externalDebitCardAccountList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        externalDebitCardAccountService.count() == 5
    }

    void "test delete"() {
        Long externalDebitCardAccountId = setupData()

        expect:
        externalDebitCardAccountService.count() == 5

        when:
        externalDebitCardAccountService.delete(externalDebitCardAccountId)
        sessionFactory.currentSession.flush()

        then:
        externalDebitCardAccountService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ExternalDebitCardAccount externalDebitCardAccount = new ExternalDebitCardAccount()
        externalDebitCardAccountService.save(externalDebitCardAccount)

        then:
        externalDebitCardAccount.id != null
    }
}
