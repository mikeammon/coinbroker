package com.coinbroker

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TokenLotServiceSpec extends Specification {

    TokenLotService tokenLotService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new TokenLot(...).save(flush: true, failOnError: true)
        //new TokenLot(...).save(flush: true, failOnError: true)
        //TokenLot tokenLot = new TokenLot(...).save(flush: true, failOnError: true)
        //new TokenLot(...).save(flush: true, failOnError: true)
        //new TokenLot(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //tokenLot.id
    }

    void "test get"() {
        setupData()

        expect:
        tokenLotService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<TokenLot> tokenLotList = tokenLotService.list(max: 2, offset: 2)

        then:
        tokenLotList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        tokenLotService.count() == 5
    }

    void "test delete"() {
        Long tokenLotId = setupData()

        expect:
        tokenLotService.count() == 5

        when:
        tokenLotService.delete(tokenLotId)
        sessionFactory.currentSession.flush()

        then:
        tokenLotService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        TokenLot tokenLot = new TokenLot()
        tokenLotService.save(tokenLot)

        then:
        tokenLot.id != null
    }
}
